extends Node

var Monster =  load("res://code/Monster.gd")
var Player =  load("res://code/Player.gd")
var Environment =  load("res://code/Environment.gd")
var Battle =  load("res://code/Battle.gd")
var Type =  load("res://code/Type.gd")
#var Item =  load("res://code/Item.gd")

var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
var envs = {
	"Village": Environment.new("Village",null,null,"res://GUI/backgrounds/env_normal.png"),
	"Desert": Environment.new("Desert",types["Fire"],types["Wind"],"res://GUI/backgrounds/env_fire.png"),
	"Forest": Environment.new("Forest",types["Wind"],types["Water"],"res://GUI/backgrounds/env_wind.png"),
	"Cemetery": Environment.new("Cemetery",types["Water"],types["Fire"],"res://GUI/backgrounds/env_water.png")
}

var env
var player 
var monster 
var battle 
var rng = RandomNumberGenerator.new()
var startFlag = false
var monsterImageAnimation 
var monsterImage 
var monsterAction
var playerAction


func _ready():
	#monsterImage = get_node('MainPanel/BackgroundPanel/MinotaurImage')
	#monsterImageAnimation = get_node('MainPanel/BackgroundPanel/MinotaurImage/MonsterAnimation')
	player = global.get_player()

	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	$MainPanel/PlayerPanel/NameLabel.text = str(player.get_name())
	$MainPanel/PlayerPanel/LevelLabel.text = "lvl " + str(player.get_lvl())		
	$MainPanel/PlayerPanel/CoinsLabel.text = "coins " + str(player.get_coins())		
	$MainPanel/PlayerPanel/HpValueLabel.text = str(player.get_hp())
	$MainPanel/PlayerPanel/AtkValueLabel.text = str(player.get_atk() - player.get_weapon().get_atkBonus()) + "+" + str(player.get_weapon().get_atkBonus())
	$MainPanel/PlayerPanel/DefValueLabel.text = str(player.get_def()- player.get_armour().get_defBonus()) + "+" + str(player.get_armour().get_defBonus())
	$MainPanel/PlayerPanel/SpdValueLabel.text = str(player.get_spd())
	$MainPanel/PlayerPanel/WeaponLabel.text = player.get_weapon().get_name() + " +" + str(player.get_weapon().get_atkBonus())
	$MainPanel/PlayerPanel/ArmourLabel.text = player.get_armour().get_name() + " +" + str(player.get_armour().get_defBonus())
	pass # Replace with function body.

### Manage Battle / Buttons Battle ###

func _on_AttackButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Attack")
	if(startFlag): 	playerAction = "atk"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " attacks!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " attacks!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
func _on_DefenseButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	if(startFlag): playerAction = "heal"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " heals himself!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " heals himself!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
func _on_ChargeButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Attack")
	if(startFlag): playerAction = "cha"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " charges!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " charges!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
		
func _on_ItemButton():
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " goes away!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/RestartBattleTimer.start()
	
func _on_MonsterTimer_timeout():
	battle.monster_action(monsterAction)
	battle_manage()
	
func _on_PlayerTimer_timeout():
	battle.player_action(playerAction)
	battle_manage()

	
func battle_manage():
	# Refresh Labels
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	monsterImageAnimation.play("Wait")
	$MainPanel/PlayerPanel/HpValueLabel.text = str(battle.get_playerCurrentHp())
	$MainPanel/MonsterPanel/HpValueLabel.text = str(battle.get_monsterCurrentHp())
	
	$MainPanel/BattleMonsterTurnPanel.visible = false
	if(startFlag): $MainPanel/BattleButtonsPanel/ItemButton.visible = false
	# Check End Battle
	if(battle.end_battle()):
		if(battle.winner() == player.get_name()):
			global.one_monster_more()
			player_reward()
			$MainPanel/TurnActiveLabel.text = player.get_name() + " won the battle!"
			monsterImageAnimation.play("Death")
		if(battle.winner() == monster.get_name()):
			global.one_death_more()
			$MainPanel/TurnActiveLabel.text = monster.get_name() + " won the battle!"
			$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Death")
			player.lose_coins(25)
		if(battle.winner() == "Draw"):
			$MainPanel/TurnActiveLabel.text = "Draw! No winner today"
		$MainPanel/RestartBattleTimer.start()
		return
		
	# Change Turn
	battle.change_turn()
	
	if(battle.get_turnActive() == player):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()-1) + ": " + player.get_name()
		$MainPanel/BattleButtonsPanel.visible = true
		pass
	if(battle.get_turnActive() == monster):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()-1) + ": " + monster.get_name()
		if(battle.get_monsterCurrentHp() < monster.get_lvl()*2):
			if(rng.randi_range(1,10) < monster.get_safeness()):
				monsterAction = "heal"
				$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " heals himself!"

			else:
				monsterAction = "atk"
				monsterImageAnimation.play("Attack")
				$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " attacks!"
		else:
			if(battle.get_monsterCurrentHp() > player.get_def()):
				if(rng.randi_range(1,10)< monster.get_aggressiveness()):
					monsterAction = "cha"
					monsterImageAnimation.play("Attack")
					$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " charges!"
				else:
					monsterAction = "atk"
					monsterImageAnimation.play("Attack")
					$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " attacks!"
			else:
					monsterAction = "atk"
					monsterImageAnimation.play("Attack")
					$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " attacks!"
		$MainPanel/BattleMonsterTurnPanel.visible = true
		$MainPanel/MonsterPanel/MonsterTimer.start()
		
	pass
	
func player_reward():
	# Monster Easy
	if(monster.get_lvl() < player.get_lvl()):
		if(monster.get_lvl()-5 < player.get_lvl()):
			pass
		else:
			player.earn_coins(5)
			
	# Monster Equal
	if(monster.get_lvl() == player.get_lvl()):
		player.lvl_up()
		player.earn_coins(50)
		
	# Monster Strong
	if(monster.get_lvl() > player.get_lvl()):
		player.lvl_up()
		player.earn_coins(100)
		
		
func monster_lvl():
	var monsterlvl 
	if(global.get_adventure() == 1):
		 monsterlvl = player.get_lvl()
		 if(monsterlvl>4): monsterlvl = 5
	if(global.get_adventure() == 2):
		monsterlvl = player.get_lvl() + rng.randi_range(-2,+2)
		if(monsterlvl>9): monsterlvl = 10
	if(global.get_adventure() == 3):
		monsterlvl = player.get_lvl() + rng.randi_range(-2,+2)
		if(monsterlvl>14): monsterlvl = 15
	if(global.get_adventure() == 4):
		monsterlvl = player.get_lvl() + rng.randi_range(-2,+2)
		if(monsterlvl>19): monsterlvl = 20
	if(global.get_adventure() == 5):
		monsterlvl = player.get_lvl() + rng.randi_range(+2,+10)
	monster.set_lvl(monsterlvl)
	pass
	
	
func _on_RestartBattleTimer_timeout():
	$MainPanel/BattleMonsterTurnPanel.visible = false
	$MainPanel/TurnActiveLabel.visible = false
	$MainPanel/MenuButtonsPanel.visible = true
	monsterImage.visible = false
	monsterImageAnimation.play("Wait")
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	$MainPanel/BackgroundPanel/BackgroundImage.texture = load(envs["Village"].get_image())
	$MainPanel/BattleButtonsPanel/ItemButton.visible = false
	startFlag = false
	# Refresh Monster Labels
	$MainPanel/MonsterPanel/NameLabel.text = "Monster ?"
	$MainPanel/MonsterPanel/HpValueLabel.text = "?"
	$MainPanel/MonsterPanel/AtkValueLabel.text = "?"
	$MainPanel/MonsterPanel/DefValueLabel.text = "?"
	$MainPanel/MonsterPanel/SpdValueLabel.text = "?"
	# Refresh Player Labels
	$MainPanel/PlayerPanel/HpValueLabel.text = str(player.get_hp())
	$MainPanel/PlayerPanel/LevelLabel.text = "lvl " + str(player.get_lvl())
	$MainPanel/PlayerPanel/HpValueLabel.text = str(player.get_hp())
	$MainPanel/PlayerPanel/AtkValueLabel.text = str(player.get_atk() - player.get_weapon().get_atkBonus()) + "+" + str(player.get_weapon().get_atkBonus())
	$MainPanel/PlayerPanel/DefValueLabel.text = str(player.get_def()- player.get_armour().get_defBonus()) + "+" + str(player.get_armour().get_defBonus())
	$MainPanel/PlayerPanel/SpdValueLabel.text = str(player.get_spd())
	$MainPanel/PlayerPanel/CoinsLabel.text = "coins " + str(player.get_coins())		
	$MainPanel/PlayerPanel/WeaponLabel.add_color_override("font_color", Color(0.19,0.13,0.08,1))		
	$MainPanel/PlayerPanel/ArmourLabel.add_color_override("font_color", Color(0.19,0.13,0.08,1))
	
### Menu Button ###

func _on_BackButton():
	global.set_player(player)
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_BattleButton():
	# Istantiate Battle
	monster = Monster.new()
	monster_lvl()
	monster.rand_monster()
	battle = Battle.new(player,monster)
	
	# Monster Animation 
	if(monster.get_race()=="Wolf"):
		monsterImage = get_node('MainPanel/BackgroundPanel/WolfImage')
		monsterImageAnimation = get_node('MainPanel/BackgroundPanel/WolfImage/MonsterAnimation')
	if(monster.get_race()=="Rat"):
		monsterImage = get_node('MainPanel/BackgroundPanel/RatImage')
		monsterImageAnimation = get_node('MainPanel/BackgroundPanel/RatImage/MonsterAnimation')
	if(monster.get_race()=="Minotaur"):
		monsterImage = get_node('MainPanel/BackgroundPanel/MinotaurImage')
		monsterImageAnimation = get_node('MainPanel/BackgroundPanel/MinotaurImage/MonsterAnimation')	
	monsterImage.visible = true
	monsterImageAnimation.play("Wait")
	
	# Refresh Interface
	$MainPanel/MenuButtonsPanel.visible = false
	$MainPanel/BattleButtonsPanel/ItemButton.visible = true
	$MainPanel/MonsterPanel/NameLabel.text = str(monster.get_name())
	$MainPanel/MonsterPanel/HpValueLabel.text = str(monster.get_hp())
	$MainPanel/MonsterPanel/AtkValueLabel.text = str(monster.get_atk())
	$MainPanel/MonsterPanel/DefValueLabel.text = str(monster.get_def())
	$MainPanel/MonsterPanel/SpdValueLabel.text = str(monster.get_spd())
	
	if(monster.get_type().get_strength() == player.get_armour().get_type().get_weakness() && player.get_armour().get_type().get_name() != "Normal"):
		#$MainPanel/PlayerPanel/ArmourLabel.font_color = Color(1,0,0)
		$MainPanel/PlayerPanel/ArmourLabel.add_color_override("font_color", Color(0,1,0))	
	if(monster.get_type().get_weakness() == player.get_armour().get_type().get_strength() && player.get_armour().get_type().get_name() != "Normal"):
		#$MainPanel/PlayerPanel/ArmourLabel.font_color = Color(0,1,0)
		$MainPanel/PlayerPanel/ArmourLabel.add_color_override("font_color", Color(1,0,0))	
	if(monster.get_type().get_strength() == player.get_weapon().get_type().get_weakness() && player.get_weapon().get_type().get_name() != "Normal"):
		#$MainPanel/PlayerPanel/WeaponLabel.font_color = Color(1,0,0)
		$MainPanel/PlayerPanel/WeaponLabel.add_color_override("font_color", Color(0,1,0))		
	if(monster.get_type().get_weakness() == player.get_weapon().get_type().get_strength() && player.get_weapon().get_type().get_name() != "Normal"):
		#$MainPanel/PlayerPanel/WeaponLabel.font_color = Color(0,1,0)
		$MainPanel/PlayerPanel/WeaponLabel.add_color_override("font_color", Color(1,0,0))
		
	# Choose random Env
	env = Environment.new()
	rng.randomize()
	env = envs[envs.keys()[rng.randi_range(0,3)]]
	$MainPanel/BackgroundPanel/BackgroundImage.texture = load(env.get_image())
	
	# Start Battle & First Turn 
	var starterFighter = battle.start_fighter()
	$MainPanel/TurnActiveLabel.visible = true
	if(starterFighter == player):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()) + ": " + player.get_name()
		$MainPanel/BattleButtonsPanel.visible = true
	else:
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()) + ": " + monster.get_name()
		$MainPanel/BattleMonsterTurnPanel.visible = true
		$MainPanel/MonsterPanel/MonsterTimer.start()
	



