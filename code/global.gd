extends Node


# Player Variables
var Player =  load("res://code/Player.gd")
var player = Player.new("Aramis",1)
#var player

# Campaign Variables
var Monster =  load("res://code/Monster.gd")
var Campaign =  load("res://code/Campaign.gd")
var Quest =  load("res://code/Quest.gd")
#var questName = ["","The Beginning", "The Ally", "The Weapon", "The Final Battle"]
#var questDescStart= ["","Hey Hunter, come here! Something is happening in the village!", "These Minotaurs will destroy the city, we need some help", "To destroy them, we need a stronger weapon: go to find and take the weapon 'The Slayer'", "It's time to finish it: kill the boss of the gang and became and take his skin!"]
#var questDescFinished = ["","It was an attack of minotaurs, we can't beat them alone...", "Thanks to god, now we have more allies!", "With this weapon, we can do it!", "The Village is safe! Now, go to explore the world and became the strongest hunter!!!"]
#var questBoss = [Monster.new(), Monster.new("Alex, The Hammer", 5 , 20, 5, 1, 0), Monster.new("David, The Wise", 8, 30, 7, 3, 0), Monster.new("Carlo, The Weaponsmith", 15, 75, 3, 5, 0), Monster.new("Tommy, The Destroyer",20 , 100, 15, 10, 0)]
#var mainQuest1 = Quest.new(questName[1], questDescStart[1], questDescFinished[1], questBoss[1])
#var mainQuest2 = Quest.new(questName[2], questDescStart[2], questDescFinished[2], questBoss[2])
#var mainQuest3 = Quest.new(questName[3], questDescStart[3], questDescFinished[3], questBoss[3])
#var mainQuest4 = Quest.new(questName[4], questDescStart[4], questDescFinished[4], questBoss[4])
var mainCampaign = Campaign.new("The Minotaurs Invasion")
var quest = 0

# Adventure Variables
var adventure = 1

# Achievs Variables
var monsterKilled = 0
var playerDeaths = 0

### Player Methods ###

func set_player(p):
	player = p
	
func get_player():
	return player

### Adventure Methods ###

func set_adventure(a):
	adventure = a

func get_adventure():
	return adventure	

### Campaign Methods ###

func set_quest(q):
	quest = q

func get_quest():
	return quest	
	
func get_campaign():
	return mainCampaign	
	
### Achievs Methods ###
	
func set_monsterKilled(m):
	monsterKilled = m
	
func one_monster_more():
	monsterKilled = monsterKilled + 1
	
func get_monsterKilled():
	return monsterKilled
	
func set_playerDeaths(m):
	playerDeaths = m
	
func one_death_more():
	playerDeaths = playerDeaths + 1
	
func get_playerDeaths():
	return playerDeaths
	
### Save & Load ###

func save_game():
	print("enter save game!")
	var config = ConfigFile.new()
	config.set_value("Profile","Player",player)
	config.set_value("Profile","adventure",adventure)
	config.set_value("Profile","quest",quest)
	config.set_value("Profile","monsterKilled",monsterKilled)
	config.set_value("Profile","playerDeaths",playerDeaths)
	config.save("res://settings.cfg")

func load_game():
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK: # If not, something went wrong with the file loading
		player = config.get_value("Profile","Player",Player.new("Aramis",1))
		adventure = config.get_value("Profile","adventure",1)
		quest = config.get_value("Profile","quest",1)
		monsterKilled = config.get_value("Profile","monsterKilled",0)
		playerDeaths = config.get_value("Profile","playerDeaths",0)
	config.save("user://settings.cfg")
