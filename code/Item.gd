extends Node

var Type =  load("res://code/Type.gd")

### Basic Bonus ###
# Name of item
var n
# Hitpoint
var hpBonus
# Attack (main source of damage)
var atkBonus
# Defense (main source to reduce damage)
var defBonus
# Speed (the initiative in the fight)
var spdBonus

### Other ###
# Part of the armory
var part
# rarity of the item (normal, uncommon, epic)
var rarity
# Home many time the item could be used (-1 it's infinites uses)
var use
# Item Type: Normal, Fire, Wind, Water
var type = Type.new()
var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
var rng = RandomNumberGenerator.new()

# Init with full stats
func _init(name="Item1",p="Weapon",h=0, a=0, d=0, s=0,t=types["Normal"]):
	n = name
	hpBonus = h
	atkBonus = a
	defBonus = d
	spdBonus = s
	part = p
	#use = u
	#rarity = r
	type = t	
	#print("Item " + n + " is ready!")
	

### Get Methods ###

func get_name():
	return n

func get_hpBonus():
	return hpBonus

func get_atkBonus():
	return atkBonus
		
func get_defBonus():
	return defBonus
	
func get_spdBonus():
	return spdBonus

func get_part():
	return part
	
func get_use():
	return use
	
func get_rarity():
	return rarity

func get_type():
	return type
	
### Get Methods ###

func set_part(p):
	part = p
	
func set_rarity(r):
	rarity = r
		
### Random Creation Methods ###

func rand_weapon(lvl=1):
	rng.randomize()
	var rndListMeleeWeapons = ["Axe","Sword","Rapier","Dagger","Saber","Mace","Spear","Pike","Scythe","Glaive","Lance","Cutlass","Dussack","Katana","Kukri","Falcata","Machete"]
	rand_type()
	set_part("Weapon")
	rand_atk(lvl)
	if((atkBonus > 5)):
		n = "Uncommon " + type.get_name() + " " + rndListMeleeWeapons[rng.randi_range(0,rndListMeleeWeapons.size()-1)] 
	if((atkBonus > 10)):
		n = "Epic " + type.get_name() + " " + rndListMeleeWeapons[rng.randi_range(0,rndListMeleeWeapons.size()-1)] 
	if((atkBonus == 15)):
		n = "Leggendary " + type.get_name() + " " + rndListMeleeWeapons[rng.randi_range(0,rndListMeleeWeapons.size()-1)] 
	else:
		n = type.get_name() + " " + rndListMeleeWeapons[rng.randi_range(0,rndListMeleeWeapons.size()-1)] 

func rand_armour(lvl=1):
	rng.randomize()
	var rndListArmour = ["Scale A.","Lamellar A.","Laminar A.", "Plated Mail", "Plate A.", "Gambeson","Chainmail","Breastplates"]
	rand_type()
	set_part("Armour")
	rand_def(lvl)
	if((defBonus > 5)):
		n = "Uncommon " + type.get_name() + " " + rndListArmour[rng.randi_range(0,rndListArmour.size()-1)] 
	if((defBonus > 10)):
		n = "Epic " + type.get_name() + " " + rndListArmour[rng.randi_range(0,rndListArmour.size()-1)]
	if((defBonus == 15)):
		n = "Leggendary " + type.get_name() + " " + rndListArmour[rng.randi_range(0,rndListArmour.size()-1)]  
	else:
		n = type.get_name() + " " + rndListArmour[rng.randi_range(0,rndListArmour.size()-1)] 


func rand_type():
	rng.randomize()
	var r = rng.randi_range(0,3)
	match r:
		0:
			type = types["Normal"]
		1:
			type = types["Fire"]
		2:
			type = types["Wind"]
		3:
			type = types["Water"]

		
func rand_stats(lvl):
	rng.randomize()
	hpBonus = rng.randi_range(1,3) * lvl
	atkBonus = rng.randi_range(1,3) * lvl
	defBonus = rng.randi_range(0,2) * lvl
	spdBonus = rng.randi_range(0,1) * lvl

func rand_atk(lvl):
	rng.randomize()
	atkBonus = rng.randi_range(1,lvl) 
	if(atkBonus>14): atkBonus = 15
	
func rand_def(lvl):
	rng.randomize()
	defBonus = rng.randi_range(1,lvl) 
	if(defBonus>14): defBonus = 15
	
func up_weapon():
	atkBonus = atkBonus+1

	
func up_armour():
	defBonus = defBonus + 1
	
