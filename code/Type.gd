extends Node

### Basic Info ###
# Name of Type
var n
# strong Element
var strength
# weak Element
var weakness



# Init with full stats
func _init(name="Normal",s=null, w=null):
	n = name
	strength = s
	weakness = w
	#print("Element " + n + " is ready!")
	

### Get Methods ###

func get_name():
	return n

func get_strength():
	return strength

func get_weakness():
	return weakness
		
### Set Methods ###

func set_name(name):
	n = name

func set_strength(s):
	strength = s

func set_weakness(w):
	weakness = w
