extends Node

var Type =  load("res://code/Type.gd")

### Basic Stats ###
# Name of monster
var n
# lvl of monster
var lvl
# Hitpoint
var hp
# Attack (main source of damage)
var atk
# Defense (main source to reduce damage)
var def
# Speed (the initiative in the fight)
var spd
# Monster Type: Normal, Fire, Wind, Water
var type = Type.new()
var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
# Type of monster (Minotaur, Wolf, Ghost)
var race
# Image of the monster
var img

### Combat Style ###

# From 1 to 10, how often the monster use Charges
var aggressiveness
# From 1 to 10, how often the monster use Heal
var safeness

var rng = RandomNumberGenerator.new()



func _init(name="Monster1",l=1, h=2, a=1, d=0, s=0, r="Wolf", t=types["Normal"]):
	n = name
	lvl = l 
	hp = h
	atk = a
	def = d
	spd = s
	type = t
	race = r
	safeness = 4
	aggressiveness = 4
	print("Monster " + n + " is ready! Hp= " + str(hp) + "; Atk: " + str(atk) + "; Def: " + str(def))

### Get Methods ###

func get_name():
	return n

func get_hp():
	return hp

func get_atk():
	return atk
		
func get_def():
	return def
	
func get_spd():
	return spd

func get_type():
	return type
	
func get_race():
	return race

func get_lvl():
	return lvl
	
func get_aggressiveness():
	return aggressiveness
	
func get_safeness():
	return safeness

### Set Methods ###

func set_lvl(l):
	lvl = l

### Random Creation Methods ###

func rand_monster():
	rand_race()
	rand_type()
	rand_stats()
	rand_name()

func rand_name():
	var rndListNames = ["Thanos","Loki","Hela","Sonny","Malekith","Corvus","Yon-Rogg","Proxima","Ebony","Nebula","Ultron","Kaecilius","Ronan"]
	n = rndListNames[rng.randi_range(0,rndListNames.size()-1)] + ", The " + type.get_name() + " " + race

func rand_type():
	rng.randomize()
	var r = rng.randi_range(0,3)
	#print("Result r in type: " + str(r))
	match r:
		0:
			type = types["Normal"]
		1:
			type = types["Fire"]
		2:
			type = types["Wind"]
		3:
			type = types["Water"]


func rand_race():
	rng.randomize()
	var r = rng.randi_range(0,2)
	#var r = 0
	match r:
		0:
			race = "Minotaur"
			n = race
			aggressiveness = 8
			safeness = 2
		1:
			race = "Wolf"
			n = race
			aggressiveness = 5
			safeness = 5
		2:
			race = "Rat"
			n = race
			aggressiveness = 2
			safeness = 8
	
func rand_stats():
	rng.randomize()
	hp = rng.randi_range(2*lvl,4*lvl)
	atk = rng.randi_range(1*lvl,2*lvl)
	def = rng.randi_range(0,1*lvl)
	#spd = rng.randi_range(0,1*lvl)
