extends Node

var Player =  load("res://code/Player.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
		
	var player = global.get_player()
	$MainPanel/PlayerNameLabel.text = player.get_name()
	$MainPanel/PlayerLevelLabel.text = "lvl " +str(player.get_lvl())
	$MainPanel/PlayerCoinsLabel.text = "coins " +str(player.get_coins())
	
	pass 


func _on_ArmoryButton():
	get_tree().change_scene("res://Scenes/Armory.tscn")
	pass # Replace with function body.


func _on_QuestButton():
	get_tree().change_scene("res://Scenes/Quest.tscn")
	pass # Replace with function body.


func _on_AdventureButton():
	get_tree().change_scene("res://Scenes/Adventure.tscn")
	pass # Replace with function body.


func _on_AchievementsButton():
	get_tree().change_scene("res://Scenes/Achievements.tscn")
	pass # Replace with function body.


func _on_HelpButton():
	get_tree().change_scene("res://Scenes/Help.tscn")
	pass # Replace with function body.


func _on_Profile_button():
	get_tree().change_scene("res://Scenes/Profile.tscn")
	pass # Replace with function body.
