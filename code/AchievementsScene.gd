extends Control

var questSelected
var questText = ["","Reach the level 50", "Kill at least 50 Monsters", "Die at least 25 times", "Finish the Quests"]
var titlesText = ["","The Master", "The Slayer", "The Walking Dead", "The Hunter"]

func _ready():
	if(global.get_player().get_lvl()>49): 
		$MainPanel/Quest1Label.text = titlesText[1] 
	if(global.get_monsterKilled()>49): 
		$MainPanel/Quest2Label.text = titlesText[2] 
	if(global.get_playerDeaths()>24):
		$MainPanel/Quest3Label.text = titlesText[3] 
	if(global.get_quest()==5):
		$MainPanel/Quest4Label.text = titlesText[4] 
	
	pass # Replace with function body.




func _on_BackButton():
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_Quest1Button():
	show_quest(1)
	#global.set_quest(1)
	pass # Replace with function body.


func _on_Quest2Button():
	show_quest(2)
	#global.set_quest(2)
	pass # Replace with function body.


func _on_Quest3Button():
	show_quest(3)
	#global.set_quest(3)
	pass # Replace with function body.


func _on_Quest4Button():
	show_quest(4)
	#global.set_quest(4)
	pass # Replace with function body.

func show_quest(i):
	$MainPanel/BackgroundImage/PageImage.visible = false
	questSelected = i
	$MainPanel/QuestTextLabel.visible = true
	$MainPanel/QuestTextLabel.text = questText[i]
	

func _on_StartQuestButton():
	get_tree().change_scene("res://Scenes/QuestBattle.tscn")
	pass # Replace with function body.


