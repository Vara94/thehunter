extends Node

var Quest =  load("res://code/Quest.gd")
var Monster =  load("res://code/Monster.gd")

# Declare member variables here. Examples:
var title
var desc
var quests = []
var currentQuest 

func _init(n="Random Campaign", d="", q = [Quest.new("Random Quest 1"),Quest.new("Random Quest 2"),Quest.new("Random Quest 3")], cq = 0):
	title = n
	desc = n
	quests = q
	currentQuest = 10
	#print("Campaign " + title + " is ready!")

### Get Methods ###

func get_name():
	return title

func get_description():
	return desc

func get_quests():
	return quests
	
func get_quest(i):
	return quests[i]
	
func get_currentQuest():
	return currentQuest
	
### Set Methods ###

func set_currentQuest(cq):
	currentQuest = cq
	
### Load Campaign ###

func load_campaign(path):
	
	# Load File
	var data_file = File.new()
	if data_file.open(path, File.READ) != OK:
		return
	var data_text = data_file.get_as_text()
	data_file.close()
	
	# Parse JSON
	var data_parse = JSON.parse(data_text)
	if data_parse.error != OK:
		return
	var data = data_parse.result
	
	title = data["campaign"]["title"]
	desc = data["campaign"]["desc"]
	currentQuest = data["campaign"]["currentQuest"] - 1
	print("Current Quest Loaded: " + str(currentQuest))
	quests = []
	for q in data["campaign"]["quests"]:
		var quest = data["campaign"]["quests"][q]
		var boss = Monster.new(quest["boss"]["name"],quest["boss"]["lvl"],quest["boss"]["hp"],quest["boss"]["atk"],quest["boss"]["def"],quest["boss"]["spd"])
		quests.append(Quest.new(quest["title"],quest["descStart"],quest["descFinish"],boss,quest["lvl"],quest["rewardCoins"]))
		
	
	pass
