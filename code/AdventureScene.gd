extends Control

var adventureSelected
var adventureText = ["","Easy monsters to start train yourself", "Harder Monster, we start!", "Mmm not bad, you are ready for something harder", "Here only the good ones", "Now it's time to test yourself until you will reach new limits: go, hunter!'"]


func _ready():
	set_adventure_level()
	if(global.get_adventure()>=2): 
		$MainPanel/Adventure2Button.visible = true
		$MainPanel/Adventure2Button/AdventureButtonImage.visible = true
		$MainPanel/Adventure2Label.visible = true
	if(global.get_adventure()>=3): 
		$MainPanel/Adventure3Button.visible = true
		$MainPanel/Adventure3Button/AdventureButtonImage.visible = true
		$MainPanel/Adventure3Label.visible = true
	if(global.get_adventure()>=4): 
		$MainPanel/Adventure4Button.visible = true
		$MainPanel/Adventure4Button/AdventureButtonImage.visible = true
		$MainPanel/Adventure4Label.visible = true
	if(global.get_adventure()==5): 
		$MainPanel/Adventure5Button.visible = true
		$MainPanel/Adventure5Button/AdventureButtonImage.visible = true
		$MainPanel/Adventure5Label.visible = true
	
	pass # Replace with function body.




func _on_BackButton():
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_Quest1Button():
	show_adventure(1)
	global.set_adventure(1)
	pass # Replace with function body.


func _on_Quest2Button():
	show_adventure(2)
	global.set_adventure(2)
	pass # Replace with function body.


func _on_Quest3Button():
	show_adventure(3)
	global.set_adventure(3)
	pass # Replace with function body.


func _on_Quest4Button():
	show_adventure(4)
	global.set_adventure(4)
	pass # Replace with function body.
	
func _on_Adventure5Button():
	show_adventure(5)
	global.set_adventure(5)
	pass # Replace with function body.


func set_adventure_level():	
	if(global.get_player().get_lvl()>4): global.set_adventure(2)
	if(global.get_player().get_lvl()>9): global.set_adventure(3)
	if(global.get_player().get_lvl()>14): global.set_adventure(4)
	if(global.get_quest()==4): global.set_adventure(5)
	#print("Quest Level: " + str(global.get_quest()) + "; Adventure Level: " + str(global.get_adventure()))
	
	
func show_adventure(i):
	$MainPanel/BackgroundImage/PageImage.visible = false
	adventureSelected = i
	$MainPanel/AdventureTextLabel.visible = true
	$MainPanel/AdventureTextLabel.text = adventureText[i]
	$MainPanel/StartAdventureButton.visible = true

func _on_StartAdventureButton():
	get_tree().change_scene("res://Scenes/Battle.tscn")
	pass # Replace with function body.
