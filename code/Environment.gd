extends Node

var Type =  load("res://code/Type.gd")

### Basic Info ###
# Name of Env
var n
# strong Element
var strongEle = Type.new()
# weak Element
var weakEle = Type.new()
# Path of the Image
var img

var rng = RandomNumberGenerator.new()

# Init with full stats
func _init(name="Env0",s=null,w=null, i="res://GUI/backgrounds/env_normal.png"):
	n = name
	strongEle = s
	weakEle = w
	img = i		
	#print("Environment " + n + " is ready!")
	

### Get Methods ###

func get_name():
	return n

func get_strongElement():
	return strongEle

func get_weakElement():
	return weakEle
		
func get_image():
	return img

func rand_env():
	rng.randomize()
	var r = rng.randi_range(0,3)
	print("Result r in Enviroment: " + str(r))
	match r:
		0:
			n = "Village"
			img = "res://GUI/backgrounds/env_normal.png"
			strongEle = Type.new("Normal","","")
			weakEle = Type.new("Normal","","")
		1:
			n = "Desert"
			img = "res://GUI/backgrounds/env_fire.png"
			strongEle = Type.new("Fire","Wind","Water")
			weakEle = Type.new("Water","Fire","Wind")
		2:
			n = "Forest"
			img = "res://GUI/backgrounds/env_wind.png"
			strongEle = Type.new("Wind","Water","Fire")
			weakEle = Type.new("Water","Fire","Wind")
		3:
			n = "Cemetery"
			img = "res://GUI/backgrounds/env_water.png"
			strongEle = Type.new("Water","Fire","Wind")
			weakEle = Type.new("Fire","Wind","Water")
