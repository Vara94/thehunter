extends Node

var Monster =  load("res://code/Monster.gd")
var Player =  load("res://code/Player.gd")
var Environment =  load("res://code/Environment.gd")
var Battle =  load("res://code/Battle.gd")
var Type =  load("res://code/Type.gd")
var Item =  load("res://code/Item.gd")

var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
var envs = {
	"Village": Environment.new("Village",null,null,"res://GUI/backgrounds/env_normal.png"),
	"Desert": Environment.new("Desert",types["Fire"],types["Wind"],"res://GUI/backgrounds/env_fire.png"),
	"Forest": Environment.new("Forest",types["Wind"],types["Water"],"res://GUI/backgrounds/env_wind.png"),
	"Cemetery": Environment.new("Cemetery",types["Water"],types["Fire"],"res://GUI/backgrounds/env_water.png")
}

# Quest Variables

var title
var descStart
var descFinished
var boss
var lvl
var rewardCoins
var rewardItem
var env

func _init(n="Random Quest", ds="", df="", b=Monster.new().rand_monster(), l=1, rc=50, ri=null, e=envs["Village"]):
	title = n
	descStart = ds
	descFinished = df
	boss = b
	lvl = l
	rewardCoins = rc
	rewardItem = ri
	env = e

	#print("Quest " + title + " is ready!")

### Get Methods ###

func get_name():
	return title

func get_descStart():
	return descStart
	
func get_descFinished():
	return descFinished

func get_boss():
	return boss
		
func get_lvl():
	return lvl
	
func get_rewardCoins():
	return rewardCoins

func get_rewardItem():
	return rewardItem
