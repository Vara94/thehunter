extends Control

var Player =  load("res://code/Player.gd")
var player
var Item =  load("res://code/Item.gd")
var item

### Manage Inventory Variables ###
var weaponInvSize = 0
var weaponInvIndex = -1
var weaponInv = []
var armourInvSize = 0
var armourInvIndex = -1
var armourInv = []

func _ready():
	player = global.get_player()
	$MainPanel/WeaponNameLabel.text = player.get_weapon().get_name()
	$MainPanel/WeaponStatsLabel.text = "+" + str(player.get_weapon().get_atkBonus())
	$MainPanel/ArmourNameLabel.text = player.get_armour().get_name()
	$MainPanel/ArmourStatsLabel.text = "+" + str(player.get_armour().get_defBonus())
	$MainPanel/CoinsValueLabel.text = str(player.get_coins())
	
	# Inventory Initialization
	inventory_init()
	


func _on_BackButton():
	global.set_player(player)
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_BuyWeapon():
	if(player.get_coins() > 99):
		player.lose_coins(100)
		item = Item.new()
		item.rand_weapon(player.get_lvl())
		player.set_weapon(item)
		player.add_item(item)
		inventory_init()
		$MainPanel/WeaponNameLabel.text = item.get_name()
		$MainPanel/WeaponStatsLabel.text = "+" + str(item.get_atkBonus())
		$MainPanel/CoinsValueLabel.text = str(player.get_coins())
	else:
		$MainPanel/CoinsLabel.text = "Not enough money: you need 100 coins!"
		$MainPanel/CoinsValueLabel.visible = false
		$MainPanel/CoinsShowTimer.start()


func _on_BuyArmor():
	if(player.get_coins() > 199):
		player.lose_coins(200)
		item = Item.new()
		item.rand_armour(player.get_lvl())
		player.set_armour(item)
		player.add_item(item)
		inventory_init()
		$MainPanel/ArmourNameLabel.text = item.get_name()
		$MainPanel/ArmourStatsLabel.text = "+" + str(item.get_defBonus())
		$MainPanel/CoinsValueLabel.text = str(player.get_coins())
	else:
		$MainPanel/CoinsLabel.text = "Not enough money: you need 200 coins!"
		$MainPanel/CoinsValueLabel.visible = false
		$MainPanel/CoinsShowTimer.start()


func _on_UpgradeWeapon():
	if(player.get_coins() > (player.get_weapon().get_atkBonus()*25-1) && player.get_weapon().get_atkBonus()>0):
		player.lose_coins(player.get_weapon().get_atkBonus()*25)
		player.get_weapon().up_weapon()
		$MainPanel/WeaponNameLabel.text = player.get_weapon().get_name()
		$MainPanel/WeaponStatsLabel.text = "+" + str(player.get_weapon().get_atkBonus())
		$MainPanel/CoinsValueLabel.text = str(player.get_coins())
	else:
		if(player.get_weapon().get_atkBonus() == 0):
			$MainPanel/CoinsLabel.text = "Buy a Weapon before!"
		else:
			$MainPanel/CoinsLabel.text = "Not enough money: you need "+ str(player.get_weapon().get_atkBonus()*25) +" coins!"
		$MainPanel/CoinsValueLabel.visible = false
		$MainPanel/CoinsShowTimer.start()
	pass

func _on_UpgradeArmour():
	if(player.get_coins() > (player.get_armour().get_defBonus()*50-1) && player.get_armour().get_defBonus()>0):
		player.lose_coins(player.get_armour().get_defBonus()*50)
		player.get_armour().up_armour()
		$MainPanel/ArmourNameLabel.text = player.get_armour().get_name()
		$MainPanel/ArmourStatsLabel.text = "+" + str(player.get_armour().get_defBonus())
		$MainPanel/CoinsValueLabel.text = str(player.get_coins())
	else:
		if(player.get_armour().get_defBonus() == 0):
			$MainPanel/CoinsLabel.text = "Buy an Armour before!"
		else:
			$MainPanel/CoinsLabel.text = "Not enough money: you need " + str(player.get_armour().get_defBonus()*50) + " coins!"
		$MainPanel/CoinsValueLabel.visible = false
		$MainPanel/CoinsShowTimer.start()
	pass # Replace with function body.


func _on_CoinsShowTimer():
	$MainPanel/CoinsValueLabel.visible = true
	$MainPanel/CoinsLabel.text = "Coins"
	pass # Replace with function body.


### Manage Inventory ###

func inventory_init():
		# Create Inventory
	var inv = player.get_inv()
	weaponInv = []
	armourInv = []
	weaponInvSize = 0
	armourInvSize = 0
	for i in inv:
		if(i.get_part()=="Weapon"):
			weaponInv.append(i)
			weaponInvSize = weaponInvSize +1
		if(i.get_part()=="Armour"):
			armourInv.append(i)
			armourInvSize = armourInvSize +1
	
	# Set Current Weapon
	var wIndex = 0
	if(weaponInvSize > 0):
		for w in weaponInv:
			print(w.get_name())
			if(w == player.get_weapon()):
				weaponInvIndex = wIndex
			wIndex = wIndex + 1
	
	# Set Current Armour
	var aIndex = 0
	if(armourInvSize > 0):
		for a in armourInv:
			if(a == player.get_armour()):
				armourInvIndex = aIndex
			aIndex = aIndex + 1
	
	# Show Arrows	
	
	### Weapon
	if(weaponInvIndex < 1):
		$MainPanel/WeaponNameLabel/WeaponLeft.visible = false
	else: 
		$MainPanel/WeaponNameLabel/WeaponLeft.visible = true
	
	if(weaponInvIndex == weaponInvSize - 1):
		$MainPanel/WeaponNameLabel/WeaponRight.visible = false
	else: 
		$MainPanel/WeaponNameLabel/WeaponRight.visible = true
	
	### Armour
	if(armourInvIndex < 1):
		$MainPanel/ArmourNameLabel/ArmourLeft.visible = false
	else: 
		$MainPanel/ArmourNameLabel/ArmourLeft.visible = true
	
	if(armourInvIndex == armourInvSize - 1):
		$MainPanel/ArmourNameLabel/ArmourRight.visible = false
	else: 
		$MainPanel/ArmourNameLabel/ArmourRight.visible = true

func _on_WeaponLeft_button():
	if(weaponInvSize > 0 && weaponInvIndex > 0):
		weaponInvIndex = weaponInvIndex - 1
		player.set_weapon(weaponInv[weaponInvIndex])
		inventory_init()
		$MainPanel/WeaponNameLabel.text = player.get_weapon().get_name()
		$MainPanel/WeaponStatsLabel.text = "+" + str(player.get_weapon().get_atkBonus())

func _on_WeaponRight_button():
	if(weaponInvSize > 0 && weaponInvIndex < weaponInvSize - 1):
		weaponInvIndex = weaponInvIndex + 1
		player.set_weapon(weaponInv[weaponInvIndex])
		inventory_init()
		$MainPanel/WeaponNameLabel.text = player.get_weapon().get_name()
		$MainPanel/WeaponStatsLabel.text = "+" + str(player.get_weapon().get_atkBonus())

func _on_ArmourLeft_button():
	if(armourInvSize > 0 && armourInvIndex > 0):
		armourInvIndex = armourInvIndex - 1
		player.set_armour(armourInv[armourInvIndex])
		inventory_init()
		$MainPanel/ArmourNameLabel.text = player.get_armour().get_name()
		$MainPanel/ArmourStatsLabel.text = "+" + str(player.get_armour().get_defBonus())
	pass # Replace with function body.


func _on_ArmourRight_button():
	if(armourInvSize > 0 && armourInvIndex < armourInvSize - 1):
		armourInvIndex = armourInvIndex + 1
		player.set_armour(armourInv[armourInvIndex])
		inventory_init()
		$MainPanel/ArmourNameLabel.text = player.get_armour().get_name()
		$MainPanel/ArmourStatsLabel.text = "+" + str(player.get_armour().get_defBonus())
	pass # Replace with function body.
