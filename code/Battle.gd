extends Node

var Monster =  load("res://code/Monster.gd")
var monster
var Player =  load("res://code/Player.gd")
var player 
var Environment =  load("res://code/Environment.gd")
var env = Environment.new()

var Type =  load("res://code/Type.gd")
var Item =  load("res://code/Item.gd")

# Declare member variables here. Examples:

var playerCurrentHp
var monsterCurrentHp
var turnActive
var turnNumber = 1
var MAX_TURNS = 25

func _init(pla, mon):
	player = pla
	playerCurrentHp = player.get_hp()
	monster = mon
	monsterCurrentHp = monster.get_hp()	
	turnActive = player
	print("Battle is ready!")

### Get Methods ###

func get_playerCurrentHp():
	return playerCurrentHp
	
func get_monsterCurrentHp():
	return monsterCurrentHp

func get_turnActive():
	return turnActive

func get_turnNumber():
	return turnNumber

### Battle Methods ###

func start_fighter():
	if(monster.get_spd()>player.get_spd()):
		turnActive = monster
		return monster
	else:
		turnActive = monster
		return player

func monster_action(act="atk"):
	if(act == "atk"):
		# Player Def Modifier
		var dmg = monster.get_atk() - player.get_def()
		if(dmg<0): dmg = 0
		# Player Armor Modifier
		if(player.get_armour().get_type().get_weakness() == monster.get_type().get_name()): 
			dmg = dmg + (monster.get_level()/2)
		if(player.get_armour().get_type().get_strength() == monster.get_type().get_name()): 
			dmg = dmg - (player.get_lvl()/2)
		if(dmg<0): dmg = 0
		playerCurrentHp = playerCurrentHp - dmg
	if(act == "heal"):
		monsterCurrentHp = monsterCurrentHp + monster.get_lvl()
		if(monsterCurrentHp > monster.get_hp()): monsterCurrentHp = monster.get_hp()
	if(act == "cha"):
		# Player Def Modifier
		var dmg = monster.get_atk()
		if(dmg<0): dmg = 0
		# Player Armor Modifier
		if(player.get_armour().get_type().get_weakness() == monster.get_type().get_name()): 
			dmg = dmg + (monster.get_level()/2)
		if(player.get_armour().get_type().get_strength() == monster.get_type().get_name()): 
			dmg = dmg - (player.get_lvl()/2)
		if(dmg<0): dmg = 0
		playerCurrentHp = playerCurrentHp - dmg
		monsterCurrentHp = monsterCurrentHp - player.get_def()
	pass
	
func player_action(act="atk"):
	# Classical Attack
	if(act == "atk"):
		# Monster Def Modifier
		var dmg = player.get_atk() - monster.get_def()
		if(dmg<0): dmg = 0
		# Monster Type modifier
		if(player.get_weapon().get_type().get_name() == monster.get_type().get_strength()):
			dmg = dmg - (monster.get_lvl()/2)
		if(player.get_weapon().get_type().get_name() == monster.get_type().get_weakness()): 
			dmg = dmg + (player.get_lvl()/2)
		if(dmg<0): dmg = 0
		monsterCurrentHp = monsterCurrentHp - dmg
	# Basic Heal
	if(act=="heal"):
		playerCurrentHp = playerCurrentHp + player.get_lvl()
		if(playerCurrentHp > player.get_hp()): playerCurrentHp = player.get_hp()
	# Charge Attack: No def modificator, but Knockback equal to monster def
	if(act == "cha"):
		var dmg = player.get_atk()
		# Monster Type modifier
		if(player.get_weapon().get_type().get_name() == monster.get_type().get_strength()):
			dmg = dmg - (monster.get_lvl()/2)
		if(player.get_weapon().get_type().get_name() == monster.get_type().get_weakness()): 
			dmg = dmg + (player.get_lvl()/2)
		if(dmg<0): dmg = 0
		monsterCurrentHp = monsterCurrentHp - dmg
		playerCurrentHp = playerCurrentHp - monster.get_def()
		pass
	pass

func end_battle():
	if (playerCurrentHp < 1 or monsterCurrentHp < 1 || turnNumber ==  MAX_TURNS):
		return true
	else:
		return false

func change_turn():
	if(turnActive == player):
		turnActive = monster
	else:
		turnActive = player
	turnNumber = turnNumber + 1
	
func winner():
	if(monsterCurrentHp > 0 && playerCurrentHp> 0):
		return "Draw"
	if (monsterCurrentHp < 1):
		return player.get_name()
	else:
		return monster.get_name()
	pass


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

