extends Panel

var Monster =  load("res://code/Monster.gd")
var monster
var Player =  load("res://code/Player.gd")
var player
var Item =  load("res://code/Item.gd")
var item
var Environment =  load("res://code/Environment.gd")
var Type =  load("res://code/Type.gd")

var types = {
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}

var envs = {
	"Village": Environment.new("Village",null,null,"res://GUI/backgrounds/env_normal.png"),
	"Desert": Environment.new("Desert",types["Fire"],types["Wind"],"res://GUI/backgrounds/env_fire.png"),
	"Forest": Environment.new("Forest",types["Wind"],types["Water"],"res://GUI/backgrounds/env_wind.png"),
	"Cemetery": Environment.new("Cemetery",types["Water"],types["Fire"],"res://GUI/backgrounds/env_water.png")
}


### Fight Stats ###
var type
# Enviroments of the combat, that change some stats
var env
# Turn of the combat: active to understand who is in turn,
# number to understand how many turn from the beginning
var turnNumber
var turnActiveName
var turnMax
# Actions done by player 
var actionDone = false
var action
# Hit Points of the player and monster
var playerCurrentHp
var monsterCurrentHp
# Utils
var exFlag

var rng = RandomNumberGenerator.new()

func _ready():	
	$PlayerPanelStats/PlayerLabel.text = "PLAYER"
	$MonsterPanelStats/MonsterLabel.text = "MONSTER"
	turnMax = 50
	player = Player.new("Rudy",1)	
	$PlayerPanelStats/PlayerLabel.text = player.get_name()
	$PlayerPanelStats/LabelWeapon.text = player.get_weapon().get_name()
	print("Fight Ready!")

####################
func main():
	
	# Until one of two enemies lose all hp
	if(playerCurrentHp > 0 && monsterCurrentHp > 0 && turnNumber < turnMax):
		# player turn
		if(turnActiveName == player.get_name()):
			print("Wait Player turn...")
		# monster turn
		else:
			monster_turn()
		#update_labels()	
	else:
		update_labels()
		# end fight, pass who won
		if (playerCurrentHp < 1):
			end_fight(monster.get_name())
		else:
			end_fight(player.get_name())

####################		
func _on_Button_Fight():
	# Set GUI
	$FightButton.visible = false
	$ShopButton.visible = false
	$Restart.visible = true
	$ActiveTurnLabel.visible = true
	$TurnNumberLabel.visible = true
	$PlayerPanelStats/PlayerActions.visible = true
	# Create  Monster
	monster = Monster.new()
	monster.rand_monster(player.get_lvl())
	$GraphicalFightPanel/MonsterSprite.visible = true
	$MonsterPanelStats/MonsterLabel.text = monster.get_name()
	# Choose random Env
	env = Environment.new()
	rng.randomize()
	env = envs[envs.keys()[rng.randi_range(0,3)]]
	$GraphicalFightPanel/BackgroundSprite.texture = load(env.get_image())
	# Set Hp
	playerCurrentHp = player.get_hp()
	monsterCurrentHp = monster.get_hp()
	$PlayerPanelStats/PlayerHpSet.text = str(playerCurrentHp)
	$MonsterPanelStats/MonsterHpSet.text = str(monsterCurrentHp)
	
	
	# Set first turn
	exFlag = true
	print("Spd Stats; Player: " + str(player.get_spd()) + "; Monster:" + str(monster.get_spd()) + ";" )
	if(monster.get_spd() > player.get_spd()):
		turnActiveName = monster.get_name()
	else:
		turnActiveName = player.get_name()
	$ActiveTurnLabel.text = turnActiveName
	turnNumber = 1
	$TurnNumberLabel.text = "Turn " + str(turnNumber)
	
	#main()

############ Turn Methods ############	
func player_turn(act):
	print("PLAYER TURN")
	if(act=="atk"):
		# Base Attack
		var dmgToMonster = player.get_atk() - monster.get_def()
		if(dmgToMonster > 0):
			monsterCurrentHp = monsterCurrentHp - dmgToMonster
	if(act=="def"):
		# Base Heal
		playerCurrentHp = playerCurrentHp + 3
		if(playerCurrentHp > player.get_hp()): playerCurrentHp = player.get_hp()
	$PlayerPanelStats/PlayerActions.visible = false
	main()
	change_turn(turnActiveName)
	
	
	return
	
func monster_turn():
	print("MONSTER TURN")
	# Base Attack
	var dmgToPlayer = monster.get_atk() - player.get_def()
	if(dmgToPlayer > 0):
		playerCurrentHp = playerCurrentHp - (monster.get_atk() - player.get_def())
	#Refresh Labels
	#update_labels()
	$TimerTurn.start()
	return

func end_fight(winner):
	print(str(turnNumber) + "  " + str(turnMax))
	#Draw Case
	if(turnNumber == turnMax):
		print(" Draw!")
		$ActiveTurnLabel.text = ("Draw!")
	else:
		print(winner + " won the fight!")
		$ActiveTurnLabel.text = (winner + " won the fight!")
		if(winner == player.get_name() && exFlag):
			# Add Experience 			
			player.lvl_up()
			# Earn Coins
			player.earn_coins(100)
			exFlag = false
		else:
			if(exFlag):
				player.lose_coins(25)
	$PlayerPanelStats/PlayerActions.visible = false
	$GraphicalFightPanel/MonsterSprite.visible = false
	$Restart.visible = true
		

func change_turn(act):
	if(act == player.get_name()):
		turnActiveName = monster.get_name()
		monster_turn()
	else:
		turnActiveName = player.get_name()
	# Next Turn
	turnNumber = turnNumber + 1	
	update_labels()	
	print("START TURN: " + str(turnNumber))
	return

func update_labels():
	# General Labels
	$ActiveTurnLabel.text = turnActiveName
	$TurnNumberLabel.text = "Turn " + str(turnNumber)
	# Player Stats Label
	$PlayerPanelStats/PlayerLabel.text = player.get_name()
	$PlayerPanelStats/PlayerLevel.text = str(player.get_lvl())
	$PlayerPanelStats/PlayerHpSet.text = str(playerCurrentHp)
	$PlayerPanelStats/PlayerAtkSet.text = str(player.get_atk())
	$PlayerPanelStats/PlayerDefSet.text = str(player.get_def())
	$PlayerPanelStats/PlayerSpdSet.text = str(player.get_spd())	
	# Monster Stats Label
	$MonsterPanelStats/MonsterLabel.text = monster.get_name()
	$MonsterPanelStats/MonsterHpSet.text = str(monsterCurrentHp)
	$MonsterPanelStats/MonsterAtkSet.text = str(monster.get_atk())
	$MonsterPanelStats/MonsterDefSet.text = str(monster.get_def())
	$MonsterPanelStats/MonsterSpdSet.text = str(monster.get_spd())

############   ############
func _on_Button_Restart():
	# Reset Player
	playerCurrentHp = player.get_hp()	
	$PlayerPanelStats/PlayerHpSet.text = str(playerCurrentHp)
	$PlayerPanelStats/PlayerCoinsValue.text = str(player.get_coins())
	# Monster Reset
	monsterCurrentHp = 1
	monster = null
	
	$PlayerPanelStats/PlayerActions.visible = false
	$FightButton.visible = true
	$ShopButton.visible = true
	$Restart.visible = false
	$ActiveTurnLabel.visible = false
	$TurnNumberLabel.visible = false
	# Monster Stats Label Unknow
	$MonsterPanelStats/MonsterLabel.text = "MONSTER"
	$MonsterPanelStats/MonsterHpSet.text = "?"
	$MonsterPanelStats/MonsterAtkSet.text = "?"
	$MonsterPanelStats/MonsterDefSet.text = "?"
	$MonsterPanelStats/MonsterSpdSet.text = "?"

func _on_TimerTurn_timeout():
	change_turn(turnActiveName)
	$PlayerPanelStats/PlayerActions.visible = true
	main()
	pass # Replace with function body.


func _on_Button_Shop():
	if(player.get_coins() > 49):
		player.lose_coins(50)
		item = Item.new()
		item.rand_weapon()
		player.set_weapon(item)
		print("Stat Weapon: hpBonus " + str(item.get_hpBonus()) +"; atkBonus " + str(item.get_atkBonus()) + "; defBonus " + str(item.get_defBonus()) + "; spdBonus " + str(item.get_spdBonus()))
		# Update GUI Weapon
		$PlayerPanelStats/LabelWeapon.text = item.get_name()
		$PlayerPanelStats/PlayerCoinsValue.text = str(player.get_coins())
		#Update GUI Players Stats
		$PlayerPanelStats/PlayerHpSet.text = str(player.get_hp())
		$PlayerPanelStats/PlayerAtkSet.text = str(player.get_atk())
		$PlayerPanelStats/PlayerDefSet.text = str(player.get_def())
		$PlayerPanelStats/PlayerSpdSet.text = str(player.get_spd())	
