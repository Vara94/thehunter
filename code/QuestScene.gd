extends Control

var questSelected

func _ready():
	global.mainCampaign.load_campaign("res://campaign.json")
	$MainPanel/CampaignLabel.text = global.mainCampaign.get_name()
	
	$MainPanel/Quest1Label.text = global.mainCampaign.get_quests()[0].get_name()
	if(global.get_quest()>=1): 
		$MainPanel/Quest2Button.visible = true
		$MainPanel/Quest2Button/QuestButtonImage.visible = true
		$MainPanel/Quest2Label.text = global.mainCampaign.get_quests()[1].get_name()
		$MainPanel/Quest2Label.visible = true
	if(global.get_quest()>=2): 
		$MainPanel/Quest3Button.visible = true
		$MainPanel/Quest3Button/QuestButtonImage.visible = true
		$MainPanel/Quest3Label.text = global.mainCampaign.get_quests()[2].get_name()
		$MainPanel/Quest3Label.visible = true
	if(global.get_quest()>=3): 
		$MainPanel/Quest4Button.visible = true
		$MainPanel/Quest4Button/QuestButtonImage.visible = true
		$MainPanel/Quest2Label.text = global.mainCampaign.get_quests()[3].get_name()
		$MainPanel/Quest4Label.visible = true
	
	pass # Replace with function body.




func _on_BackButton():
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_Quest1Button():
	show_quest(0)
	#global.set_quest(1)
	pass # Replace with function body.


func _on_Quest2Button():
	show_quest(1)
	#global.set_quest(2)
	pass # Replace with function body.


func _on_Quest3Button():
	show_quest(2)
	#global.set_quest(3)
	pass # Replace with function body.


func _on_Quest4Button():
	show_quest(3)
	#global.set_quest(4)
	pass # Replace with function body.

func show_quest(i):
	$MainPanel/BackgroundImage/PageImage.visible = false
	questSelected = i
	$MainPanel/QuestTextLabel.visible = true
	if(global.get_quest() == i): $MainPanel/QuestTextLabel.text = global.mainCampaign.get_quest(i).get_descStart()
	else: $MainPanel/QuestTextLabel.text = global.mainCampaign.get_quest(i).get_descFinished()
	if(global.get_quest() == i): $MainPanel/StartQuestButton.visible = true
	else: $MainPanel/StartQuestButton.visible = false

func _on_StartQuestButton():
	get_tree().change_scene("res://Scenes/QuestBattle.tscn")
	pass # Replace with function body.


