extends Node

var Monster =  load("res://code/Monster.gd")
var Player =  load("res://code/Player.gd")
var Environment =  load("res://code/Environment.gd")
var Battle =  load("res://code/Battle.gd")
var Type =  load("res://code/Type.gd")
var Item =  load("res://code/Item.gd")

var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
var envs = {
	"Village": Environment.new("Village",null,null,"res://GUI/backgrounds/env_normal.png"),
	"Desert": Environment.new("Desert",types["Fire"],types["Wind"],"res://GUI/backgrounds/env_fire.png"),
	"Forest": Environment.new("Forest",types["Wind"],types["Water"],"res://GUI/backgrounds/env_wind.png"),
	"Cemetery": Environment.new("Cemetery",types["Water"],types["Fire"],"res://GUI/backgrounds/env_water.png")
}

var env
var player 
var monster 
var battle 
var rng = RandomNumberGenerator.new()
var startFlag = false
var monsterAction
var playerAction


func _ready():
	player = global.get_player()
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	$MainPanel/QuestActiveLabel.text = "Quest " + str(global.get_quest()+1) + " - " + global.get_campaign().get_name()
	$MainPanel/PlayerPanel/NameLabel.text = str(player.get_name())
	$MainPanel/PlayerPanel/LevelLabel.text = "lvl " + str(player.get_lvl())		
	$MainPanel/PlayerPanel/CoinsLabel.text = "coins " + str(player.get_coins())		
	$MainPanel/PlayerPanel/HpValueLabel.text = str(player.get_hp())
	$MainPanel/PlayerPanel/AtkValueLabel.text = str(player.get_atk() - player.get_weapon().get_atkBonus()) + "+" + str(player.get_weapon().get_atkBonus())
	$MainPanel/PlayerPanel/DefValueLabel.text = str(player.get_def()- player.get_armour().get_defBonus()) + "+" + str(player.get_armour().get_defBonus())
	$MainPanel/PlayerPanel/SpdValueLabel.text = str(player.get_spd())
	$MainPanel/PlayerPanel/WeaponLabel.text = player.get_weapon().get_name() + " +" + str(player.get_weapon().get_atkBonus())
	$MainPanel/PlayerPanel/ArmourLabel.text = player.get_armour().get_name() + " +" + str(player.get_armour().get_defBonus())
	pass # Replace with function body.

### Manage Battle / Buttons Battle ###

func _on_AttackButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Attack")
	if(startFlag): 	playerAction = "atk"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " attacks!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " attacks!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
func _on_DefenseButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	if(startFlag): playerAction = "heal"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " heals himself!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " heals himself!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
func _on_ChargeButton():
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Attack")
	if(startFlag): playerAction = "cha"
	else: 
		startFlag = true
		$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " charges!"
		battle_manage()
	#battle_manage()
	$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = player.get_name() + " charges!"
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = true
	$MainPanel/PlayerPanel/PlayerTimer.start()
	
	
func _on_MonsterTimer_timeout():
	battle.monster_action(monsterAction)
	battle_manage()

func _on_PlayerTimer():
	battle.player_action(playerAction)
	battle_manage()

	
func battle_manage():
	# Refresh Labels
	$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Wait")
	$MainPanel/BackgroundPanel/MonsterImage/MonsterAnimation.play("Wait")
	$MainPanel/PlayerPanel/HpValueLabel.text = str(battle.get_playerCurrentHp())
	$MainPanel/MonsterPanel/HpValueLabel.text = str(battle.get_monsterCurrentHp())
	$MainPanel/BattleButtonsPanel.visible = false
	$MainPanel/BattleMonsterTurnPanel.visible = false
	
	# Check End Battle
	if(battle.end_battle()):
		if(battle.winner() == player.get_name()):
			$MainPanel/TurnActiveLabel.text = player.get_name() + " won the battle! Quest Passed!"
			player.earn_coins(100*global.get_quest())
			if(global.get_quest()==3 && player.get_weapon().get_atkBonus() < 13):
				var item = Item.new("The Slayer","Weapon",0,12,0,0)
				player.add_item(item)
				player.set_weapon(item)
			if(global.get_quest()==4 && player.get_armour().get_defBonus() < 16):
				var item = Item.new("The Mino Skin","Armour",0,0,12,0)
				player.add_item(item)
				player.set_armour(item)
				
			$MainPanel/BackgroundPanel/MonsterImage/MonsterAnimation.play("Death")
		if(battle.winner() == monster.get_name()):
			$MainPanel/TurnActiveLabel.text = monster.get_name() + " won the battle! Quest Lost"
			$MainPanel/BackgroundPanel/PlayerImage/PlayerAnimation.play("Death")
		if(battle.winner() == "Draw"):
			$MainPanel/TurnActiveLabel.text = "Draw! No winner today"
		$MainPanel/RestartBattleTimer.start()
		return
		
	# Change Turn
	battle.change_turn()
	
	if(battle.get_turnActive() == player):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()-1) + ": " + player.get_name()
		$MainPanel/BattleButtonsPanel.visible = true
	if(battle.get_turnActive() == monster):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()-1) + ": " + monster.get_name()
		if(battle.get_monsterCurrentHp() < 3):
			if(rng.randi_range(1,10)<4):
				monsterAction = "heal"
				$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " heal himself!"

			else:
				monsterAction = "atk"
				$MainPanel/BackgroundPanel/MonsterImage/MonsterAnimation.play("Attack")
				$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " attacks!"
		else:
			monsterAction = "atk"
			$MainPanel/BackgroundPanel/MonsterImage/MonsterAnimation.play("Attack")
			$MainPanel/BattleMonsterTurnPanel/WaitMonsterTurn.text = monster.get_name() + " attacks!"
		$MainPanel/BattleMonsterTurnPanel.visible = true
		$MainPanel/MonsterPanel/MonsterTimer.start()
		
	pass
	
func _on_RestartBattleTimer_timeout():	

	if(battle.winner() == player.get_name()): global.set_quest(global.get_quest() + 1)
	get_tree().change_scene("res://Scenes/Quest.tscn")	


### Menu Button ###

func _on_BackButton():
	global.set_player(player)
	get_tree().change_scene("res://Scenes/Quest.tscn")
	pass # Replace with function body.


func _on_BattleButton():
	# Istantiate Quest Battle
	monster = global.mainCampaign.get_quest(global.quest).get_boss()
	battle = Battle.new(player,monster)
	
	# Refresh Interface
	$MainPanel/BackgroundPanel/MonsterImage.visible = true
	$MainPanel/BackgroundPanel/MonsterImage/MonsterAnimation.play("Wait")
	$MainPanel/MenuButtonsPanel.visible = false
	$MainPanel/MonsterPanel/NameLabel.text = str(monster.get_name())
	$MainPanel/MonsterPanel/HpValueLabel.text = str(monster.get_hp())
	$MainPanel/MonsterPanel/AtkValueLabel.text = str(monster.get_atk())
	$MainPanel/MonsterPanel/DefValueLabel.text = str(monster.get_def())
	$MainPanel/MonsterPanel/SpdValueLabel.text = str(monster.get_spd())
	
	# Start Battle & First Turn 
	var starterFighter = battle.start_fighter()
	$MainPanel/TurnActiveLabel.visible = true
	if(starterFighter == player):
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()) + ": " + player.get_name()
		$MainPanel/BattleButtonsPanel.visible = true
	else:
		$MainPanel/TurnActiveLabel.text = "Turn " + str(battle.get_turnNumber()) + ": " + monster.get_name()
		$MainPanel/BattleMonsterTurnPanel.visible = true
		$MainPanel/MonsterPanel/MonsterTimer.start()
	
func quest_monster(i):
	#if(i==1): monster = Monster.new("Dummy, The Rat", 1, 1, 0, 0)
	if(i==1): monster = Monster.new("Alex, The Hammer", 5 , 20, 5, 1, 0)
	if(i==2): monster = Monster.new("David, The Wise", 8, 30, 7, 3, 0)
	if(i==3): monster = Monster.new("Carlo, The Weaponsmith", 15, 75, 3, 5, 0)
	if(i==4): monster = Monster.new("Tommy, The Destroyer",20 , 100, 15, 10, 0)
	pass


