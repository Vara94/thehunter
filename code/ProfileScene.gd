extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$MainPanel/ProfileLabel/NameLabel/ValueLabel.text = global.get_player().get_name()
	$MainPanel/StatisticsLabel/LevelLabel/Value.text = str(global.get_player().get_lvl())
	$MainPanel/StatisticsLabel/MonsterKilledLabel/Value.text = str(global.get_monsterKilled())
	$MainPanel/StatisticsLabel/DeathsLabel/Value.text = str(global.get_playerDeaths())
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BackButton():
	get_tree().change_scene("res://Scenes/Main.tscn")
	pass # Replace with function body.


func _on_ChangeButton():
	$MainPanel/ProfileLabel/NameLabel/NewNameTextbox.text = global.player.get_name()
	$MainPanel/ProfileLabel/NameLabel/NewNameButton.visible = true
	$MainPanel/ProfileLabel/NameLabel/NewNameTextbox.visible = true
	pass # Replace with function body.


func _on_NewNameButton():
	global.player.set_name($MainPanel/ProfileLabel/NameLabel/NewNameTextbox.text)
	$MainPanel/ProfileLabel/NameLabel/ValueLabel.text = global.player.get_name()
	$MainPanel/ProfileLabel/NameLabel/NewNameButton.visible = false
	$MainPanel/ProfileLabel/NameLabel/NewNameTextbox.visible = false
	pass # Replace with function body.


func _on_SaveButton():
	global.save_game()
	#
	pass # Replace with function body.


func _on_LoadButton():
	global.load_game()
	$MainPanel/ProfileLabel/NameLabel/ValueLabel.text = global.get_player().get_name()
	$MainPanel/StatisticsLabel/LevelLabel/Value.text = str(global.get_player().get_lvl())
	$MainPanel/StatisticsLabel/MonsterKilledLabel/Value.text = str(global.get_monsterKilled())
	$MainPanel/StatisticsLabel/DeathsLabel/Value.text = str(global.get_playerDeaths())
	pass # Replace with function body.
