extends Node

### Basic Stats ###
# Name of player
var n
# Hitpoint
var hp
# Attack (main source of damage)
var atk
# Defense (main source to reduce damage)
var def
# Speed (the initiative in the fight)
var spd
# Level (1 every 100 points of experience)
var lvl
# Experience, used to managed the level progression
var ex

### Items ###
var Item =  load("res://code/Item.gd")
var Type = load("res://code/Type.gd")
var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
# Inventory (all the items possessed by a player)
var inv
# Money
var coins
# Items Used
var weapon 
var armour
#var helmet = "empty"
#var boot = "empty"
#var pants = "empty"
#var necklace = "empty"
#var ring1 = "empty"
#var ring2 = "empty"

# Init with full stats
func _init(name="Player1", l=1, h=12, a=2, d=1, s=0):
	n = name
	hp = h
	atk = a
	def = d
	spd = s
	lvl = l
	ex = 0
	inv = []
	weapon = Item.new("Rusty Sword","Weapon",0,0,0,0,types["Normal"])
	armour = Item.new("Cloth Robe","Armour",0,0,0,0,types["Normal"])
	coins = 0
	#print("Player " + n + " is ready! Hp= " + str(hp) + "; Atk: " + str(atk) + "; Def: " + str(def))
	

### Get Methods ###

func get_name():
	return n

func get_hp():
	return hp 

func get_atk():
	return atk + weapon.get_atkBonus()
		
func get_def():
	return def + armour.get_defBonus()
	
func get_spd():
	return spd 

func get_lvl():
	return lvl

func get_coins():
	return coins
	
func get_weapon():
	return weapon

func get_armour():
	return armour
	
func get_inv():
	return inv
	
### Get Methods ###

func set_name(nam):
	n = nam
	
### Experience Progression ###
# Update experience calculate if the player level up
func update_ex(e):
	ex = ex + e
	if(ex > 99):
		ex = ex - 100
		lvl_up()

func lvl_up():
	lvl = lvl + 1
	print("Level Up! Current level: " + str(lvl))
	update_stats_lvl(lvl)

	
func update_stats_lvl(l):
	hp = 10 + 2*l # 5 + 5 lifepoints every 1 level
	atk = 1 + 1*l # 1 + 1 attack every 1 level
	def = 1 + int(lvl/4) # 1 defense every 4 levels
	spd = int(lvl/2) # 1 speed every 2 levels
	
### Inventory Management ###

func earn_coins(i):
	coins = coins + i

func lose_coins(i):
	if(coins > (i - 1)):
		coins = coins - i

func add_item(i):
	if(inv.find(i) == -1):
		inv.append(i)
		
func remove_item(i):
	var index = inv.find(i)
	if(inv.find(i) != -1):
		inv.remove(index)

func set_weapon(w):
	# if you have the item in your inventory
	#if(inv.find(w) == -1 && w.get_part() == 'Weapon'):
	weapon = w
	# Update Stats
	hp = hp 
	atk = atk
	def = def 
	spd = spd 
	
func set_armour(w):
	# if you have the item in your inventory
	#if(inv.find(w) == -1 && w.get_part() == 'Weapon'):
	armour = w
	# Update Stats
	hp = hp 
	atk = atk
	def = def 
	spd = spd 


