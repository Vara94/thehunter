# The Hunter - version 1.0
## Introduction
### Core
The Hunter is an RPG game about the adventures of an hunter. The core of the game is the hunting of fantastic beast. The player has to improve his armory, fights against the powerful beasts in the world and became the most important hunter in the world 
### Theme
The theme of the game is fantasy medieval. The main story is about save a village from the invasion of horrible minotaurs. After the main quest, the hunter has to fight against the others hunters and beasts in the world and became the strongest one
### Features
This game has several features:
- Fight in a turn-based combat against different creatures in different environments
- Improve your skills trough new weapons and armories
- Use different style of combat and skills to win your enemies
- Save your village trough the main quest
- Play new campaigns created by the community

## Mechanics
### State
The main entity of the game is the **Player**, that has the following characteristics:

> Name: name of the player
> lvl: level of the player
> hp: life points of the player
> atk: the damage made by the player
> def: the defense of the player
> spd: the speed of the player
> weapon: the weapon of the player
> armour: the armour of the player

The `def` is used as a reducer of the damage by the opponents. The damage (`atk` of the opponents for _Base Attack_). if `def > atk`, the damage will be 0.
The `spd` is used to calculate who starts to attack.

the second most important entity is the **Monster**, the opponent that the player has to fights. It is described as following:

> Name: name of the monster
> lvl: level of the monster
> hp: life points of the monster
> atk: the damage made by the monster
> def: the defense of the monster
> spd: the speed of the monster
> type: the type of the monster
> race: the race of the monster
> aggressiveness: behaviour feature
> safeness: behaviour feature

The differences from the player are the `type` and the `race`. The `race` in this version are: Minotaurs, Rats and Wolves. The `type` is the elemental modifier of the Monster. This feature is present in the **Item** too. The type of the weapon affects the damage inflicted by the player to the monster, while the type of the armour affects the damage inflicted by the monster to the player. The Types are the following:

>Normal: No Weakness, No Strength
>Fire: Water Weakness, Wind Strength
>Water: Wind Weakness, Fire Strength
>Wind: Fire Weakness, Water Strength

The entites described above are involved in the **Battle**, that manage the fight between the player and the monster. The Battle is turn-based (with a limit defined in `MAX_TURNS`, set to at 25).
In every turn, the two opponents choose an action. The common actions are the _Attack_ , _Charge_ and _Heal_: the first is an attack on the opponents (the damage is calculated from base attack, weapon modifier and type), the second is like the _Attack_ but ignore the `def` of the opponent and take damage equal to the opponent's `def`. The last one, the _Heal_,  is an heal of an half of the level of the entity. The exclusive action of the player is _Go Away_: only in the first turn in a battle in _Adventure_, the player could go away from the monster without lose money (a defeat in a battle make a lost of 25 coins for the player). While the actions of the avatar are chosen by the player, the monsters chose the actions based on their behaviours feature and some other variables:

> Charge ->`if(battle.get_monsterCurrentHp() < monster.get_lvl()*2)`
> Heal -> `if(battle.get_monsterCurrentHp() > player.get_def())`

If one of these states are respected, there is a probability (based on the _aggressiveness_ or the _safeness_) that the monster make a _Charge_ or an _Heal_. The normal case, is the base _Attack_ 

There are two different ways to wage a battle: via _Adventure_ or via _Quest_. The first way, is a battle with random encounters, divided in 5 different stage:

> Trial Camp: from 1st to 5th level
> The Cemetery: from 3rd to 7th level
> The Forest: from 9th to 17th level
> The Desert: from 13th to 22th level
> The Endless: from 19th to infinite level

From this battles, the player earn money, that can spends in the _Armory_ to buy new items or upgrade the old ones

The second way is a short campaign, divided in 4 stages:
> Stage 1 - The Beginning (recommended level 5)
> Stage 2 - The Ally (recommended level 8)
> Stage 3 - The Weapon (recommended level 15)
> Stage 4 - The Final Battle (recommended level 20)

All the stages have coins as reward. In addition, Stage 3 has a weapon as reward (_The Slayer_) and Stage 4 has a armour as reward (_The Mino Skin_).

It's possible to upload new campaigns from a json file _campaign.json_. Right now, is possible to create a campaign with a maximum of 5 quests.

The **Item** gives bonus to the player. In this version, are implemented the weapons (that give atk bonus) and the armour (that give def bonus). The items are stored in an inventory. The weapon and/or the armor can be changed at the _Armory_

### Avatar
The avatar of the player is Aramis, the hunter. The name can be changed at the _Profile_ menu

### Initial State
the initial state is the _Main_ scene, where the player is created, with the following stats:

> Name = Aramis
> lvl = 1
> hp = 10
> atk = 2
> def = 1
> spd = 0
> weapon =  Rusty Sword (+0 Normal)
> armour = Cloth Robe (+0 Normal)

### Progress 
The progress in the game has 2 different way:

> Finish the Campaign
> Complete the Achievements

The Campaign was already described above: the goal is complete the 4 quests.
The Achievements are 4 in this version: _The Master_(reach level 50), _The Slayer_(kill 50 monster), _The Walking Dead_(die 25 times), _The Hunter_(finish the campaign).
After the end of these two goals, the game could go on, and has technically infinity longevity

### Views
There are different views of the game:
- _**Main**_: the main menu of the game, used to reach the different views

- _**Help**_: a short description of the game

- _**Achievements**_: a menu where the player could check his achievements

- _**Armory**_: a menu where the player could upgrade or buy new item. The possible actions are the following:

  _Buy Weapon_: a random weapon is gave to the player at the cost of 100 coins. The weapon has an atk bonus from +1 to the level of the player (with a max bonus of 15). The weapon has type and based on the bonus can be Uncommon (bonus >5), Epic (bonus >10) or Legendary (bonus =15)

  _Buy Armour_: a random armour is gave to the player at the cost of 200 coins. The armour has a def bonus from +1 to the level of the player (with a max bonus of 15). The armour has type and based on the bonus can be Uncommon (bonus >5), Epic (bonus >10) or Legendary (bonus =15)

  _Upgrade Weapon_: add +1 to the atk bonus of the weapon. The cost depends from the previous bonus of the weapon: atkBonus*25 coins

  _Upgrade Armour_: add +1 to the def bonus of the armour. The cost depends from the previous bonus of the armour: defBonus*50 coins

- _**Quest**_: a menu where the the player can follow the campaign. It is possible to choose a quest only if the previous one is completed

- _**Adventure**_: a menu where the player can start to fight against random encounters. The stages are level-based, except the last one, that is unlocked only if the player has finished the campaign

- _**Profile**_: a menu where the users can load or save the game and change the name of the avatar

- _**Battle**_: the views of the combat, reachable from the _Adventure_ or _Quest_. Here there are the combat, between the avatar and the monsters. There are the actions that the player could choose and the animations of them (the monster has animations too)

## Next Version Features
The main feature that can be added is the improvement of the behaviour of the monsters, with the implementations of the behaviour trees. The behaviour tree for the current behaviour of the monsters is the following



```mermaid
graph TD
START[Sequence] --> A[How many HP i have?]
START[Sequence]  --> B[Am i aggressive?]
START[Sequence] --> C[Selector]
C --> D[Heal]
C --> E[Charge]
C --> F[Heal]


```

Others features, besides the graphic ones, will be the modifier of the Type of the Enviroment, that affect the Battle. In the end, the adding of more achievements, in order to adding longevity to the game