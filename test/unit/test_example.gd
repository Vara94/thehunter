extends "res://addons/gut/test.gd"

var Monster =  load("res://code/Monster.gd")
var Player =  load("res://code/Player.gd")
var Environment =  load("res://code/Environment.gd")
var Battle =  load("res://code/Battle.gd")
var Type =  load("res://code/Type.gd")
var Item =  load("res://code/Item.gd")

var types = {
	"Normal": Type.new("Normal","",""),
	"Fire": Type.new("Fire","Wind","Water"),
	"Wind": Type.new("Wind","Water","Fire"),
	"Water": Type.new("Water","Fire","Wind")
}
var envs = {
	"Village": Environment.new("Village",null,null,"res://GUI/backgrounds/env_normal.png"),
	"Desert": Environment.new("Desert",types["Fire"],types["Wind"],"res://GUI/backgrounds/env_fire.png"),
	"Forest": Environment.new("Forest",types["Wind"],types["Water"],"res://GUI/backgrounds/env_wind.png"),
	"Cemetery": Environment.new("Cemetery",types["Water"],types["Fire"],"res://GUI/backgrounds/env_water.png")
}

func before_each():
	gut.p("ran setup", 2)

func after_each():
	gut.p("ran teardown", 2)

func before_all():
	gut.p("ran run setup", 2)

func after_all():
	gut.p("ran run teardown", 2)

func test_standard_player():
	var player = Player.new("Aramis", 1, 12, 2, 1, 0)
	# Level Progression
	player.lvl_up()
	assert_eq(player.get_lvl(),2,"Standard Player 1")
	# Coins
	player.earn_coins(100)
	assert_eq(player.get_coins(),100,"Standard Player 2")
	
func test_standard_monster():
	var monster = Monster.new()
	# Creation
	monster.rand_monster()
	assert_gt(monster.get_hp(),0,"Standard Monster 1")
	
func test_standard_battle():
	var player = Player.new("Aramis", 1, 12, 2, 1, 0)
	var monster = Monster.new("Dummy",1, 2, 1, 0, 0, "Wolf", types["Normal"])
	var battle = Battle.new(player,monster)
	# Battle
	var start = battle.start_fighter()
	if(start == player):
		battle.player_action("atk")
	else:
		battle.monster_action("atk")
	assert_eq(battle.winner(),player.get_name(),"Standard Battle 1")
	assert_true(battle.end_battle(),"Standard Battle 1")
