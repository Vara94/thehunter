extends Node


### Basic Bonus ###
# Name of item
var n
# Hitpoint
var hpBonus
# Attack (main source of damage)
var atkBonus
# Defense (main source to reduce damage)
var defBonus
# Speed (the initiative in the fight)
var spdBonus

### Other ###
# Type of the armory
var type
# rarity of the item (normal, uncommon, epic)
var rarity
# Home many time the item could be used (-1 it's infinites uses)
var use



# Init with full stats
func _init(name="Item1",h=5, a=1, d=0, s=0,t="Weapon",u=3,r="Normal"):
	n = name
	hpBonus = h
	atkBonus = a
	defBonus = d
	spdBonus = s
	type = t
	use = u
	rarity = r
	
	print("Item " + n + " is ready!")
	

### Get Methods ###

func get_name():
	return n

func get_hpBonus():
	return hpBonus

func get_atkBonus():
	return atkBonus
		
func get_defBonus():
	return defBonus
	
func get_spdBonus():
	return spdBonus

func get_type():
	return type
	
func get_use():
	return use
	
func get_rarity():
	return rarity
